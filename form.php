<div class="container">
    <!-- SECTION FORM -->
    <h1>Regístrate</h1>
    <form name="formCountries">
        <!-- COUNTRIES OF THE WORLD -->
        <div class="form-group">
            <div class="form-group">
                <label for="exampleFormControlSelect1">País</label>
                <select name="myselect" class="form-control" id="exampleFormControlSelect1"
                        onclick="selectCities()">
                    <option selected="true" disabled="disabled">Selecciona tu país</option>
                    <?php
                    foreach ($resultsCountries as $packageCountries) {
                        foreach ($packageCountries as $countries) {
                            ?>
                            <option name="optionCountry"><?= $countries->countryName ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
        </div>
        <label for="exampleFormControlSelect1">Ciudades</label>
        <!-- BASE FORM -->
        <select class="form-control" id="baseForm">
            <option selected="true" disabled="disabled">Selecciona tu ciudad</option>
            <option value="">...</option>
        </select>
        <!-- CITIES FROM COLOMBIA -->
        <select class="form-control" id="selectColombia" style="display: none">
            <option selected="true" disabled="disabled">Selecciona tu ciudad</option>
            <?php
            foreach ($resultCitiesCol as $packageCities) {
                foreach ($packageCities as $city) {
                    ?>
                    <option value=""><?= $city->name ?></option>
                    <?php
                }
            }
            ?>
        </select>
        <!-- CITIES FROM ANDORRA -->
        <select class="form-control" id="selectAndorra" style="display: none">
            <option selected="true" disabled="disabled">Selecciona tu ciudad</option>
            <?php
            foreach ($resultsCitiesAD as $packageCities) {
                foreach ($packageCities as $city) {
                    ?>
                    <option value=""><?= $city->name ?></option>
                    <?php
                }
            }
            ?>
        </select>
        <!-- CITIES FROM ARGENTINA -->
        <select class="form-control" id="selectArgentina" style="display: none">
            <option selected="true" disabled="disabled">Selecciona tu ciudad</option>
            <?php
            foreach ($resultsCitiesAR as $packageCities) {
                foreach ($packageCities as $city) {
                    ?>
                    <option value=""><?= $city->name ?></option>
                    <?php
                }
            }
            ?>
        </select>
        <!-- CITIES FROM AMERICAN SAMOA -->
        <select class="form-control" id="selectAmericanSamoa" style="display: none">
            <option selected="true" disabled="disabled">Selecciona tu ciudad</option>
            <?php
            foreach ($resultsCitiesAS as $packageCities) {
                foreach ($packageCities as $city) {
                    ?>
                    <option value=""><?= $city->name ?></option>
                    <?php
                }
            }
            ?>
        </select>
        <!-- CITIES FROM AUSTRIA -->
        <select class="form-control" id="selectAustria" style="display: none">
            <option selected="true" disabled="disabled">Selecciona tu ciudad</option>
            <?php
            foreach ($resultsCitiesAT as $packageCities) {
                foreach ($packageCities as $city) {
                    ?>
                    <option value=""><?= $city->name ?></option>
                    <?php
                }
            }
            ?>
        </select>
        <!-- CITIES FROM AUSTRALIA -->
        <select class="form-control" id="selectAustralia" style="display: none">
            <option selected="true" disabled="disabled">Selecciona tu ciudad</option>
            <?php
            foreach ($resultsCitiesAU as $packageCities) {
                foreach ($packageCities as $city) {
                    ?>
                    <option value=""><?= $city->name ?></option>
                    <?php
                }
            }
            ?>
        </select>
    </form>
</div>
<!-- SECTION JS -->
<script>
    function selectCities() {
        let result = document.formCountries.myselect.value
        //Form base
        if (result == "Selecciona tu ciudad") {
            document.getElementById("baseForm").style.display = "block";
        } else {
            document.getElementById("baseForm").style.display = "none";
        }
        //Cities from Colombia
        if (result == "Colombia") {
            document.getElementById("selectColombia").style.display = "block";
        } else {
            document.getElementById("selectColombia").style.display = "none";
        }
        //Cities from Andorra
        if (result == "Andorra") {
            document.getElementById("selectAndorra").style.display = "block";
        } else {
            document.getElementById("selectAndorra").style.display = "none";
        }
        //Cities from Argentina
        if (result == "Argentina") {
            document.getElementById("selectArgentina").style.display = "block";
        } else {
            document.getElementById("selectArgentina").style.display = "none";
        }
        //Cities from American Samoa
        if (result == "American Samoa") {
            document.getElementById("selectAmericanSamoa").style.display = "block";
        } else {
            document.getElementById("selectAmericanSamoa").style.display = "none";
        }
        //Cities from Austria
        if (result == "Austria") {
            document.getElementById("selectAustria").style.display = "block";
        } else {
            document.getElementById("selectAustria").style.display = "none";
        }
        //Cities from Austria
        if (result == "Australia") {
            document.getElementById("selectAustralia").style.display = "block";
        } else {
            document.getElementById("selectAustralia").style.display = "none";
        }
    }
</script>
