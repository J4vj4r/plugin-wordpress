# Plugin Wordpress de un formulario de países y ciudades

[![formulariobespoken.png](https://i.postimg.cc/Rh8pVryW/formulariobespoken.png)](https://postimg.cc/CnGsm2F0)

___
Este plugin fue creado para mostrar un formulario de países y ciudades
el cual se alimenta de una API llamada geonames, en su primer versión tiene cargadas 6 bases de datos de ciudades con
sus respectivos países y una última con el país de Colombia.
