
<?php
/*
 * Plugin Name: plugin-form
 * Description: form responsive with find countries and cities
 * Author: Javier Jaramillo
 * Shortcode [plugin-form]
 * Version: 0.1
 */


add_shortcode("plugin_form", "plugin_form");

function plugin_form()
{
//find countries for api
    $urlCountry = 'http://api.geonames.org/postalCodeCountryInfoJSON?username=ingenierodesoftware7';
//find cities for api for each country code
    $urlCitiesCol = 'http://api.geonames.org/searchJSON?country=CO&maxRows=100&username=ingenierodesoftware7';
    $urlCitiesAD = 'http://api.geonames.org/searchJSON?country=AD&maxRows=100&username=ingenierodesoftware7';
    $urlCitiesAR = 'http://api.geonames.org/searchJSON?country=AR&maxRows=100&username=ingenierodesoftware7';
    $urlCitiesAS = 'http://api.geonames.org/searchJSON?country=AS&maxRows=100&username=ingenierodesoftware7';
    $urlCitiesAT = 'http://api.geonames.org/searchJSON?country=AT&maxRows=100&username=ingenierodesoftware7';
    $urlCitiesAU = 'http://api.geonames.org/searchJSON?country=AU&maxRows=100&username=ingenierodesoftware7';
    $urlCitiesAX = 'http://api.geonames.org/searchJSON?country=AX&maxRows=100&username=ingenierodesoftware7';

    $arguments = array(
        'method' => 'GET',
    );
    //GROUP OF COUNTRIES
    $responseCountries = wp_remote_get($urlCountry, $arguments);    //Only countries
    //GROUP OF CITIES
    $responseCitiesCol = wp_remote_get($urlCitiesCol, $arguments);  //Colombia
    $responseCitiesAD = wp_remote_get($urlCitiesAD, $arguments);    //Andorra
    $responseCitiesAR = wp_remote_get($urlCitiesAR, $arguments);    //Argentina
    $responseCitiesAS = wp_remote_get($urlCitiesAS, $arguments);    //American Samoa
    $responseCitiesAT = wp_remote_get($urlCitiesAT, $arguments);    //Austria
    $responseCitiesAU = wp_remote_get($urlCitiesAU, $arguments);    //Australia
    $responseCitiesAX = wp_remote_get($urlCitiesAX, $arguments);    //Åland

    if (is_wp_error(
            $responseCountries ||
            $responseCitiesCol ||
            $responseCitiesAD  ||
            $responseCitiesAR ||
            $responseCitiesAS ||
            $responseCitiesAT ||
            $responseCitiesAU
    )) {
        $error_message = $response->get_error_message();
        return "Hubo un error: $error_message";
    }
    //Group countries and cities
    $resultsCountries = json_decode(wp_remote_retrieve_body($responseCountries));
    $resultCitiesCol = json_decode(wp_remote_retrieve_body($responseCitiesCol));
    $resultsCitiesAD = json_decode(wp_remote_retrieve_body($responseCitiesAD));
    $resultsCitiesAR = json_decode(wp_remote_retrieve_body($responseCitiesAR));
    $resultsCitiesAS = json_decode(wp_remote_retrieve_body($responseCitiesAS));
    $resultsCitiesAT = json_decode(wp_remote_retrieve_body($responseCitiesAT));
    $resultsCitiesAU = json_decode(wp_remote_retrieve_body($responseCitiesAU));
    include 'form.php'
    ?>
    <?php
}









